
---
title: "Forks"
date: 2016-03-25
---

* * * *

Imaginons que Julien propose un dépôt public, visible par tout le monde, mais
non modifiable (pour éviter que n'importe qui y fasse n'importe quoi).  Si
Fabien (à qui il arrive parfois de faire n'importe quoi) veut proposer une
modification, il peut alors copier (forker) le dépôt de Julien, faire ses
modifications dans son dépôt, puis les soumettre à Julien (pull request), qui
choisira ou non de les intégrer dans le dépôt initial (`upstream`).

Le fork est un mode de fonctionnement très répandu dans le monde de
l'open-source (Github entre autres).  Le serveur Gogs de l'université possède
également une partie publique propice aux forks.

Remarque : pour les intervenants réguliers et de confiance, il est plus simple
de les autoriser à modifier directement le dépôt principal, en les ajoutant
comme collaborateurs. Les deux systèmes (forks et collaboration) sont
complémentaires. 

# Forker un dépôt distant

Le dépôt de Julien est déjà sur le serveur, dans la partie publique. Fabien se
connecte sur le site Gogs de l'université.  

![](forks_01.png)

*  *  *  *

Il explore la zone publique et sélectionne le dépôt de Julien.

![](forks_02.png)

*  *  *  *

Il demande de forker le projet.

![](forks_03.png)

*  *  *  *

Ce qui lui crée un nouveau dépôt...

![](forks_04.png)

*  *  *  *

... qui est une copie du dépôt initial.

![](forks_05.png)

*  *  *  *

Fabien peut ensuite cloner son nouveau dépôt...

![](forks_06a.png)

... y faire ses modifications ...

![](forks_06b.png)

... et les pusher sur son dépôt distant.

![](forks_06c.png)

# Envoyer un pull request

Un pull request permet de demander d'intégrer d'une modification d'un dépôt
forké dans le dépôt initial (`upstream`).

Jusqu'ici, Fabien a forké le dépôt de Julien et fait des modifications sur son
dépôt forké. Pour soumettre ses modifications, il va sur la page correspondant
à son dépôt et clique sur le bouton pull request.

![](forks_07.png)

*  *  *  *

Il met un message pour Julien, expliquant ses modifications (ou pas d'ailleurs).

![](forks_08.png)

*  *  *  *

Le pull request est alors enregistré.

![](forks_09.png)

# Gérer un pull request

De son côté, Julien reçoit le pull request de Fabien, pour le dépôt initial.

![](forks_j10.png)

*  *  *  *

Il peut alors regarder les modifications proposées, échanger des messages avec
Fabien puis finalement accepter (ou refuser) les modifications.

![](forks_j11.png)

*  *  *  *

Si le pull request est accepté, la modification est fusionnée dans le dépôt.

![](forks_j12.png)

*  *  *  *

Elle apparait alors dans la liste des commits. 

![](forks_j13.png)

Si les modifications du fork ont été faites dans une nouvelle branche, il ne
faut pas oublier de la fusionner dans le `master`, sur le dépôt `upstream`.

En cas de conflit, le pull request doit être intégré manuellement (voir ci-après).

# Mettre à jour un dépôt forké, spécifier manuellement les dépots distants

Après un fork, les deux dépôts (fork et upstream) peuvent évoluer
indépendamment.  Pour récupérer les nouvelles modifications du dépôt initial
dans le dépôt forké, il suffit de l'ajouter comme dépôt distant (généralement,
on utilise le nom `upstream`). 

Par exemple, Fabien peut ajouter le dépôt initial (de Julien) sous le nom
`upstream`  puis mettre à jour son dépôt forké en récupérant les nouvelles modifications du dépôt initial `upstream`.

```
# ajoute un dépôt distant sous le nom upstream :
git remote add upstream https://gogs.univ-littoral.fr/jdehos/tutoriel_git

# récupère et fusionne les nouvelles modifications de upstream :
git fetch upstream
git merge upstream/master
```

*  *  *  *

Cette gestion des dépôts distants est très puissante. Elle permet, par exemple,
de créer, pour un même projet, un dépôt distant public et un dépôt distant privé
(éventuellement sur des serveurs différents) et de les synchroniser. 

Autre exemple, Julien peut gérer manuellement un pull request de Fabien dans 
une branche spéciale. Pour cela, il lui suffit d'ajouter et de récupérer le
dépôt distant de Fabien (par exemple sous le nom `fork_fabien`) et de créer une
branche (`pull_request_fabien`) pour y merger le `master` de `fork_fabien`.

```
# ajoute le dépôt distant (fork), sous le nom fork_fabien :
git remote add fork_fabien https://gogs.univ-littoral.fr/fteytaud/tutoriel_git

# récupère les nouvelles modifications de fork_fabien :
git fetch fork_fabien

# fusionne ces modifications dans une nouvelle branche pull_request_fabien :
git branch pull_request_fabien
git checkout pull_request_fabien
git merge fork_fabien/master
```


# Résumé 

| |
---|---|
`git remote add <nom> <url>` | ajoute un dépôt distant |
`git fetch <nom>` | récupère les commits d'un dépôt distant |

# Conseils

- Ajoutez les membres du projet comme collaborateurs du dépôt distant.
- Pour les dépôts publics, utilisez les pull requests pour récupérer les
  éventuelles contributions de la communauté.

# Exercice 1

- Associez-vous à un ou deux collègues : l'un d'entre vous doit créer un dépôt
  public, les autres doivent le forker et soumettre des pull-requests.

# Exercice 2

- Créez deux dépôts distants, l'un publique, l'autre privé.
- Clonez le dépôt privé, faites-y quelques commits et pushez le tout.
- Toujours dans le dépôt privé local, créez une branche `public`, fusionnez-y
  le `master` et pushez-la sur le dépôt distant privé et sur le dépôt distant
public.
- Faites un nouveau commit dans le `master`, fusionnez-le dans la branche
  `public` et pushez le tout.
- Clonez le dépôt public et mettez-le à jour avec la branche `public` du dépôt
  distant privé.

