
---
title: "Dépôt distant"
date: 2016-03-31
---

* * * *

Git permet de synchroniser un dépôt local avec un dépôt distant (sur un
serveur).  Ceci permet d'envoyer les commits locaux sur le serveur et,
réciproquement, de récupérer les commits du serveur dans un dépôt local.
Lorsqu'un dépôt local est synchronisé avec un serveur, Git crée et utilise une
étiquette prédéfinie supplémentaire (`origin/master`) pour indiquer le dernier
commit du serveur.

*  *  *  *

Il existe des serveurs comme Github qui permettent d'héberger gratuitement des
dépôts publics (visibles par tout le monde).  Le serveur Gogs de l'université
vous permet d'héberger des dépôts publics mais également des dépôts privés.  Il
dispose d'une page web vous permettant de gérer vos projets. Pour cela, allez à
l'adresse [https://gogs.univ-littoral.fr](https://gogs.univ-littoral.fr) et
entrez vos identifiants :

![](depot_distant_01.png)

*  *  *  *

Une fois identifié(e), le site vous affiche une page d'accueil (derniers
commits, dépôts actifs...) :

![](depot_distant_02.png)

# Créer un dépôt sur un serveur Git

Pour créer un nouveau dépôt distant, allez sur le site du serveur Gogs et
cliquez « Nouveau dépôt ».

![](depot_distant_03.png)

*  *  *  *

Entrez le nom du dépôt distant à créer, puis cliquez « Créer un dépôt ».

![](depot_distant_04.png)

*  *  *  *

Le dépôt distant est alors créé et une page vous indique comment le récupérer
localement. Attention, il y a une méthode plus simple que celle indiquée (cf.
[section suivante](#cloner-un-dépôt-distant-vers-un-nouveau-dépôt-local)).

![](depot_distant_05.png)

*  *  *  *

Le site du serveur Gogs vous permet de configurer différents paramètres
concernant votre dépôt. Par exemple pour un dépôt privé, vous pouvez indiquer
les personnes que vous autorisez à récupérer et à modifier le dépôt (cliquez
« Paramètres » puis « Collaboration » puis entrez et ajoutez le login).

![](depot_distant_15.png)

# Cloner un dépôt distant vers un nouveau dépôt local

La commande `git clone ...` permet de récupérer un dépôt distant sur votre
machine. C'est la méthode la plus simple pour récupérer un dépôt. 
Pour éviter d'avoir à retaper votre login sans arrêt, pensez à l'ajouter après
le « `https://` » lors du clone (vous pouvez également l'ajouter plus tard, dans le
fichier `.git/config`). 

![](depot_distant_06.png)

# Synchroniser un dépôt local existant vers un dépôt distant

Si vous avez déjà créé et modifié un dépôt local... 

![](depot_distant_07a.png)

![](depot_distant_07a.svg)

*  *  *  *

... alors vous pouvez le synchroniser avec un dépôt distant.  Pour cela, il
faut lancer les commandes :

1. `git remote add origin ...`  (pour ajouter, dans le
dépôt local, un dépôt distant qu'on appellera `origin`) ;
2. `git push -u origin master`  (pour envoyer, dans le dépôt distant `origin`, la
branche `master` du dépôt local).

![](depot_distant_07b.png)

![](depot_distant_07b.svg)

Ces deux commandes synchronisent le dépôt
distant et le dépôt local ; il n'est plus nécessaire de les relancer ensuite.

# Récupérer (tirer) les commits d'un dépôt distant

La commande `git pull` permet de récupérer les éventuelles modifications sur le
serveur (par exemple envoyées par un collaborateur ou par vous-même depuis une
autre machine) et de les intégrer dans votre dépôt local.

![](depot_distant_08.png)

![](depot_distant_08.svg)

# Envoyer (pousser) les commits locaux sur un dépôt distant

Après des commits locaux...

![](depot_distant_09a.png)

![](depot_distant_09a.svg)

*  *  *  *

... la commande `git push` permet d'envoyer vos commits locaux sur le serveur.

![](depot_distant_09b.png)

![](depot_distant_09b.svg)

*  *  *  *

Les commits/fichiers envoyés sur le serveur sont alors visibles sur la page web.

![](depot_distant_10.png)

*  *  *  *

Vous pouvez également voir le contenu de votre dépôt avec un client Git
graphique.

![](depot_distant_11.png)

# Résoudre des conflits

Imaginons que vous avez modifié et commité un fichier, et que vous voulez
l'envoyer sur le serveur. Pour cela, il faut d'abord faire un `git pull` avant
le `git push` au cas où l'un de vos collaborateurs aurait envoyé une
modification sur le serveur entre-temps. 

En effet, si quelqu'un a commité et pushé des modifications en parallèle, alors
le `master` du serveur pointe sur un commit différent du `master` local.

![](depot_distant_12a.svg)

*  *  *  *

Il faut donc fusionner ces modifications, c'est-à-dire créer un nouveau commit
rassemblant les modifications de `master` et de `origin/master` puis
envoyer ce nouveau commit sur le serveur.  Git est souvent capable de faire ces
fusions automatiquement mais parfois il n'arrive pas à résoudre le conflit et
il vous demande de le faire manuellement.  Voici ce que ça peut donner, dans le
cas d'une fusion avec conflit : 

![](depot_distant_12.png)

*  *  *  *

On est donc dans un état non commité et contenant un conflit à résoudre.

![](depot_distant_12b.svg)

*  *  *  *

Pour résoudre le conflit, il suffit d'ouvrir le fichier en cause et de
remplacer les zones marquées par le contenu que vous voulez obtenir. 
Il existe des outils graphiques comme `meld` qui peuvent vous y aider.

![](depot_distant_13.png)

*  *  *  *

Une fois le fichier corrigé, vous pouvez commiter...

![](depot_distant_14a.png)

![](depot_distant_14a.svg)

*  *  *  *

... et pusher vers le serveur.

![](depot_distant_14b.png)

![](depot_distant_14b.svg)

# Résumé 

| |
---|---|
`git clone` | récupère un dépôt distant |
`git pull` | récupère les commits du dépôt distant dans le dépôt local |
`git push` | envoie les commits du dépôt local sur le dépôt distant |

# Conseils

- Utilisez la page <https://gogs.univ-littoral.fr> pour créer et
gérer vos dépôts distants.
- Récupérez vos dépôts distants avec `git clone`.
- Récupérez les modifications du serveur régulièrement (avec `git pull`).
- Faites un `git pull` avant d'envoyer vos nouveaux commits sur le serveur (avec `git push`).

# Exercice 1

- Créez un dépôt sur le serveur Gogs et récupérez-le dans un dépôt local.
- Ajoutez/commitez/pushez quelques fichiers et vérifiez sur le site Gogs que
  les modifications sont bien sur le serveur.

# Exercice 2

- Associez-vous à un ou deux collègues.
- Créez un dépôt sur le serveur Gogs et ajoutez vos collègues comme collaborateurs.
- Récupérez votre dépôt et ajoutez/commitez/pushez quelques fichiers.
- Demandez à vos collègues de récupérer votre projet.
- Demandez à un collègue de commiter/pusher une modification et vérifiez que
  vous arrivez à la récupérer de votre côté.
- Faites maintenant des modifications en parallèle sur un même fichier.
- Vérifiez que vous avez bien un conflit, résolvez-le et synchronisez tout le
  monde.

