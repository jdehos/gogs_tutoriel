---
title: "Introduction"
date: 2016-03-31
---

# À qui s'adresse ce tutoriel ?
- Objectif du tutoriel : apprendre à utiliser l'outil Git et le [serveur
  Gogs](https://gogs.univ-littoral.fr) mis en place par le
[SCOSI](http://scosi.univ-littoral.fr/) à l'[ULCO](http://univ-littoral.fr). 
- Pré-requis : utilisation basique d'un ordinateur (et notamment du clavier).
- Public visé : toute personne ayant à manipuler, journaliser ou partager des
  fichiers de texte brut (txt, LaTeX, python, matlab, R, java, CSV...). 

# Qu'est-ce-que Git et Gogs ?
- Git : système décentralisé de journalisation de fichiers (alternative à
  mercurial, subversion, CVS...)
- Gogs : service d'hébergement de fichiers utilisable avec Git (alternative
  à Github, Bitbucket...)

# Dans quels cas utiliser Git/Gogs ?
Git est conçu pour manipuler principalement des fichiers **au format texte**
(code source, code LaTeX, fichier CSV...). Attention, les fichiers word, excel
et PDF ne sont pas des fichiers au format texte et Git est beaucoup moins
intéressant pour manipuler ce genre de fichiers.

Quelques fonctionnalités/utilisations de Git/Gogs :

- **journalisation et branches** : « je modifie souvent mes fichiers et je peux
  revenir à mes modifications précédentes, les annuler ou en tester plusieurs
en parallèle »
- **sauvegarde distante** : « quand je le souhaite, l'outil envoie une copie de
  sauvegarde de mes fichiers sur un serveur ».
- **synchronisation** : « je travaille sur plusieurs postes différents et je
  peux synchroniser mes fichiers facilement entre les postes ».
- **travail en équipe** : « mes collègues et moi travaillons sur des fichiers
  communs ; les modifications de chacun sont retransmises aux autres ; les
éventuels conflits sont détectés ».
- **projets publics ou privés** : « je peux créer des projets publics, visibles
  par tout le monde, et des projets privés, visibles et modifiables par les personnes
que j'indique ».

# Dans quels cas ne pas utiliser Git/Gogs ?
- édition collaborative en temps-réel → sharelatex
- partage de fichiers « au plus simple » → dropbox, seafile, serveur FTP/HTTP...
- fichiers dans un format non textuel (word, excel, PDF...) 

# Notion de « version » (commit)

Un « projet Git » contient l'historique de toutes les modifications sauvegardées
de chaque fichier.  On appelle « commit » un état sauvegardé de ces modifications
(une version du projet en quelque sorte).

*  *  *  *

Généralement, on modifie le projet à partir de la version précédente et ansi de
suite ; ce qui correspond à une succession de commits :

![](concepts_commits_1.svg)

On peut également effectuer des modifications en parallèles. On appelle ça
des branches :

![](concepts_commits_2.svg)

On peut ensuite rassembler ces modifications en fusionnant les branches :

![](concepts_commits_3.svg)

# Notion de dépôt (repository)

On appelle dépôt tout l'historique des fichiers et des modifications de notre
« projet Git », c'est-à-dire l'ensemble des commits.

*  *  *  *

Un dépôt peut être dupliqué et synchronisé sur différentes machines, ce qui
permet de partager un projet et pouvoir travailler à plusieurs.  Git est un
système décentralisé, c'est-à-dire que chaque dépôt correspondant au projet
contient toutes les informations du projet. Ainsi, chaque dépôt peut se
synchroniser avec n'importe quel autre dépôt du projet.

<img src="concepts_depots_git.svg" style="width:600px">

Pour simplifier le déroulement du projet, on met généralement un dépôt sur un
serveur distant et on synchronise les autres dépôts via ce dépôt distant. C'est
le mode de fonctionnement du serveur Gogs du SCOSI, de Github...

# Références

- [http://git-scm.com/book/fr/v2](http://git-scm.com/book/fr/v2)
- [https://www.atlassian.com/git/tutorials/](https://www.atlassian.com/git/tutorials/)

