WWW_ROOT = ~/public-html/tutoriel_git

.PHONY: all publish clean

all: $(WWW_ROOT)
	$(MAKE) -C www WWW_ROOT=$(WWW_ROOT)
	$(MAKE) -C installation WWW_ROOT=$(WWW_ROOT)
	$(MAKE) -C branches WWW_ROOT=$(WWW_ROOT)
	$(MAKE) -C depot_distant WWW_ROOT=$(WWW_ROOT)
	$(MAKE) -C depot_local WWW_ROOT=$(WWW_ROOT)
	$(MAKE) -C forks WWW_ROOT=$(WWW_ROOT)

$(WWW_ROOT):
	mkdir -p $(WWW_ROOT)

publish: all
	scp -r $(WWW_ROOT) yangra.univ-littoral.fr:public-html/

clean:
	rm -rf $(WWW_ROOT)
	$(MAKE) -C www clean
	$(MAKE) -C installation clean
	$(MAKE) -C branches clean
	$(MAKE) -C depot_distant clean
	$(MAKE) -C depot_local clean
	$(MAKE) -C forks clean

